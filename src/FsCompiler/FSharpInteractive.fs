namespace Workshop.FsCompiler

open Microsoft.FSharp.Compiler.SourceCodeServices
open Microsoft.FSharp.Compiler.Interactive.Shell
open Microsoft.FSharp.Compiler
open Microsoft.FSharp.Compiler.SourceCodeServices
open System
open System.IO
open System.Text

module FSharpInteractive =

  type Warning = {
    Message : string
    Line : int
    Column : int
  }

  let private toWarning (errInfo : FSharpErrorInfo) =
    {
      Message = errInfo.Message
      Line = errInfo.StartLineAlternate
      Column = errInfo.StartColumn
    }

  let private checker = FSharpChecker.Create()

  let toolTip input funcName = async {
    let file = "./test.fsx"
    let! projOptions =
      checker.GetProjectOptionsFromScript(file, input)
    let! parseFileResults =
        checker.ParseFileInProject(file, input, projOptions)
    let! checkFileAnswer =
      checker.CheckFileInProject(parseFileResults, file, 0, input, projOptions)
    let checkFileResults =
      match checkFileAnswer with
      | FSharpCheckFileAnswer.Succeeded(res) -> Some res
      | res ->
        printfn "Parsing did not finish... (%A)" res
        None
    let identToken = FSharpTokenTag.Identifier
    match checkFileResults with
    | Some res ->
      let! tip = res.GetToolTipTextAlternate(0, 0, "", [funcName], identToken)
      match tip with
      | FSharpToolTipText elems ->
        match List.tryHead elems with
        | Some elem ->
          match elem with
          | FSharpToolTipElement.Single (text,_) ->
            let sign = text.Replace("Full name: Test." + funcName, "").Trim()
            return Some sign
          | _ -> return None
        | None -> return None
    | _ -> return None
  }

  let private sbOut = new StringBuilder()
  let private sbErr = new StringBuilder()
  let private inStream = new StringReader("")
  let private outStream = new StringWriter(sbOut)
  let private errStream = new StringWriter(sbErr)
  let private fsiPath = Environment.GetEnvironmentVariable "FSI_FILE_PATH"
  let private args = Array.append [|fsiPath|] [|"--noninteractive"|]

  let private fsiConfig = FsiEvaluationSession.GetDefaultConfiguration()
  let private fsiSession = FsiEvaluationSession.Create(fsiConfig, args, inStream, outStream, errStream)

  let private eval f text =
      let result, fsharpErrorInfos = f text
      let warnings = fsharpErrorInfos |> Seq.map toWarning
      if Seq.isEmpty warnings then
        match result  with
        | Choice1Of2 value ->
          Choice1Of3 value
        | Choice2Of2 (ex : Exception) ->
          Choice3Of3 ex
      else
        Choice2Of3 warnings


  let evalInteraction = eval fsiSession.EvalInteractionNonThrowing
  let evalExpression = eval fsiSession.EvalExpressionNonThrowing