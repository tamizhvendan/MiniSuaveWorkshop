namespace Worshop.Runner
open Fake
open Workshop.Runner.Steps
open System.IO
open Workshop.FsCompiler.FSharpInteractive

module Program =

  let steps : StepsConfig.Step list ref = ref []

  let handleChanges e =
    let dojoSource =
      File.ReadAllText "MiniSuave.fsx"
    let currentSteps = !steps
    if List.isEmpty currentSteps then
      printSuccess stepsConfig.End
    else
      printfn "compiling..."
      System.Threading.Thread.Sleep(2000)
      if (evalStep dojoSource currentSteps.Head) then
        printSuccess currentSteps.Head.Success
        steps := currentSteps.Tail
        if List.isEmpty !steps then
          printfn "%s" stepsConfig.End
        else
          let step = !steps
          printStep step.Head
      else
        ()

  let executeInlineScript script =
    printfn "executing..."
    match evalExpression script with
    | Choice1Of3 result ->
      match result with
      | Some v ->
        printfn "%A" v.ReflectionValue
      | None -> printfn "<NoResponse>"
    | Choice2Of3 warnings ->
      printWarnings warnings
    | Choice3Of3 ex ->
      printError ex.Message


  [<EntryPoint>]
  let main argv =
    stepsConfig.Message |> printfn "\n\n\n\t\t\t\t%s\n\n\n"
    if Array.isEmpty argv then
      steps := start 0
    else
      steps := start ((int argv.[0]) - 1)
    let steps = !steps
    printStep steps.Head
    use watcher = !! "*.fsx" |> WatchChanges handleChanges
    let mutable continueLooping = true
    while continueLooping do
      let script = System.Console.ReadLine()
      if script = "\q" then
        continueLooping <- false
      else
        executeInlineScript script
    watcher.Dispose()
    0
