type HttpMethod = Get | Post | Put
type Request = {
  Method : HttpMethod
  Path : string
}
type StatusCode = Ok | NotFound | BadRequest
let toStatusCode statusCode =
  match statusCode with
  | Ok -> "200"
  | NotFound -> "404"
  | BadRequest -> "400"
type Response = {
  StatusCode : StatusCode
  Content : string
}
type Context = {
  Request : Request
  Response : Response
}
type WebPart = Context -> Async<Context option>