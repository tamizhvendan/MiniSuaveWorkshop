namespace Workshop.Runner
open FSharp.Data
open Workshop.FsCompiler.FSharpInteractive
open System.IO
open System

module Steps =

  type StepsConfig = JsonProvider<"./steps.json">

  let stepToString (step : StepsConfig.Step) =
    let header = sprintf "Step {%d} : %s" step.Id step.Description
    let lineBreak = "-----------------------------------------------------------"
    let hint =
      if Option.isSome step.Hint then
        sprintf "Hint: %s" (step.Hint.Value)
      else
        ""
    sprintf "%s\n%s\n%s\n%s\n" lineBreak header hint lineBreak

  let printStep (step : StepsConfig.Step) = step |> stepToString |> printfn "%s"

  let stepsConfig = StepsConfig.GetSample()

  let getStepFromConfig (config : StepsConfig.Root) id  =
    config.Steps |> Array.filter (fun s -> s.Id = id) |> Array.head

  let printWarnings warnings =
    Console.ForegroundColor <- ConsoleColor.Yellow
    printfn "Fix the following errors"
    warnings
    |> Seq.iter (fun w -> printfn "%s at %d,%d" w.Message w.Line w.Column)
    Console.ResetColor()

  let printError msg =
    Console.ForegroundColor <- ConsoleColor.Red
    printfn "%s" msg
    Console.ResetColor()

  let printSuccess msg =
    Console.ForegroundColor <- ConsoleColor.Green
    printfn "%s" msg
    Console.ResetColor()



  let evalTestExpression source (testExpression : StepsConfig.TestExpression) =
    let expr = testExpression.Expected
    match testExpression.ToolTip with
    | Some value ->
      let sign = toolTip source testExpression.Expected |> Async.RunSynchronously
      match sign with
      | Some sign ->
        if sign = value then true
        else
          sprintf "Expected `%s` but found `%s`" value sign |> printError
          false
      | None ->
        sprintf "Expected `%s` but found none" value |> printError
        false
    | None ->
      match evalExpression expr with
      | Choice1Of3 result ->
        match testExpression.It with
        | Some expectedValue ->
          // Workaround for JSON int
          let expected = expectedValue.Trim('"')
          match result with
          | Some v ->
            if string(v.ReflectionValue) = expected then
              true
            else
              sprintf "[%s] => Expected %s But Found %A" expr expected (v.ReflectionValue)
              |> printError
              false
          | None -> true
        | _ -> true
      | Choice2Of3 warnings ->
        if testExpression.Error = "CERR" then
          printWarnings warnings
          false
        else
          printError testExpression.Error
          false
      | Choice3Of3 ex ->
        sprintf "FATAL[Config] : Exception %s" ex.Message
        |> printError
        false


  let rec assertStep source (testExpressions : StepsConfig.TestExpression list) =
    match testExpressions with
    | [] -> true
    | x::xs ->
      if evalTestExpression source x then
        assertStep source xs
      else
        false

  let evalStep source (step : StepsConfig.Step) =
    match evalInteraction source with
    | Choice1Of3 _ ->
      step.TestExpressions |> Array.toList |> assertStep source
    | Choice2Of3 warnings ->
      printWarnings warnings
      false
    | Choice3Of3 ex ->
      sprintf "Fatal : %s" ex.Message
      |> printError
      false

  let start skipStep =
    stepsConfig.Steps
    |> Array.skip skipStep
    |> Array.toList